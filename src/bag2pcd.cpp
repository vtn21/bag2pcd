#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/PointCloud2.h>
#include <boost/foreach.hpp>

#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/registration/icp.h>

#include <iostream>
#include <cstdlib>

int main(int argc, char **argv) {
	if(argc != 4) {
		std::cerr << "Usage: rosrun bag2pcd bag2pcd <bag_path> <pcd_path> <number_of_clouds>" << std::endl;
	}
	else {
		//Opening bag and velodyne_points topic
		rosbag::Bag bag;
		bag.open(argv[1], rosbag::bagmode::Read);
		std::vector<std::string> topics;
		topics.push_back(std::string("/velodyne_points"));
		rosbag::View view(bag, rosbag::TopicQuery(topics));
		///////////////////////////////////////
		
		//Final cloud containing the whole bag
		pcl::PointCloud<pcl::PointXYZI>::Ptr final_cloud (new pcl::PointCloud<pcl::PointXYZI>);
				
		int counter = 0;
		BOOST_FOREACH(const rosbag::MessageInstance m, view) {
			std::cout << "Examining message #" << counter << std::endl;
			
			//Opening the ROS Message and converting to PCL PointCloud
			pcl::PointCloud<pcl::PointXYZI>::Ptr input_cloud (new pcl::PointCloud<pcl::PointXYZI>);
			pcl::fromROSMsg(*(m.instantiate<sensor_msgs::PointCloud2>()), *input_cloud);
			
			if(counter == 0) {
				//Start with the bag's first cloud
				final_cloud = input_cloud;
			}
			else if(counter == std::atoi(argv[3])) break; //Limiting number of PointClouds
			else {
				//Declaring an Iterative Closest Point object
				pcl::IterativeClosestPoint<pcl::PointXYZI, pcl::PointXYZI> icp;
				icp.setInputSource(input_cloud);
				icp.setInputTarget(final_cloud);
				//Temp cloud to receive the previous 2 clouds aligned
				pcl::PointCloud<pcl::PointXYZI> temp_cloud;
				//Making the alignment
				icp.align(temp_cloud);
				
				//Check convergence
				if(icp.hasConverged()) {
					final_cloud->width += temp_cloud.width;
					int i = 0;
					BOOST_FOREACH(pcl::PointXYZI point, temp_cloud.points) {
						final_cloud->points.push_back(temp_cloud.points[i]);
						i++;
					}
				}
				else {
					std::cerr << "Iterative Closest Point failed!" << std::endl;
					bag.close();
					return -1;
				}
			}
			counter++;
		}
		bag.close();
		
		//Save a PCD on the specified location
		pcl::io::savePCDFileASCII(argv[2], *final_cloud);
		std::cout << "Bag successfully converted to PCD. See file " << argv[2] << "." << std::endl;	
	}
	return 0;
}


