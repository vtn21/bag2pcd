#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/PointCloud2.h>
#include <boost/foreach.hpp>

#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>

#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>

#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>

#define ARCH_DISTANCE 3.5
#define ARCH_ANGLE 25

int main(int argc, char **argv) {
	if(argc != 4) {
		std::cerr << "Usage: rosrun bag2pcd bag2pcd <bag_path> <pcd_path> <number_of_clouds>" << std::endl;
	}
	else {		
		//Opening bag and velodyne_points topic
		rosbag::Bag bag;
		bag.open(argv[1], rosbag::bagmode::Read);
		std::vector<std::string> topics;
		topics.push_back(std::string("/velodyne_points"));
		rosbag::View view(bag, rosbag::TopicQuery(topics));
		///////////////////////////////////////
		
		//Final cloud containing the whole bag
		pcl::PointCloud<pcl::PointXYZI>::Ptr final_cloud (new pcl::PointCloud<pcl::PointXYZI>);
				
		int counter = 0;
		BOOST_FOREACH(const rosbag::MessageInstance m, view) {
			std::cout << "Examining message #" << counter << std::endl;
			
			//Opening the ROS Message and converting to PCL PointCloud
			pcl::PointCloud<pcl::PointXYZI>::Ptr input_cloud (new pcl::PointCloud<pcl::PointXYZI>);
			pcl::fromROSMsg(*(m.instantiate<sensor_msgs::PointCloud2>()), *input_cloud);
			
			//Aux cloud to perform filtering
			pcl::PointCloud<pcl::PointXYZI>::Ptr aux_cloud (new pcl::PointCloud<pcl::PointXYZI>);
			*aux_cloud = *input_cloud;
			aux_cloud->points.clear();
			aux_cloud->width = 0;
			
			//Filtering
			for(int i = 0; i < input_cloud->width; i++) {
				if(!(input_cloud->points[i].x < 2.3 && input_cloud->points[i].x > -2.152 && std::abs(input_cloud->points[i].y) < 0.8605)) {
					if(!(pow(input_cloud->points[i].x, 2) + pow(input_cloud->points[i].y, 2) <= pow(ARCH_DISTANCE, 2) && abs(input_cloud->points[i].y) <= input_cloud->points[i].x * tan(ARCH_ANGLE * M_PI / 180) && input_cloud->points[i].x > 0)) {
						aux_cloud->points.push_back(input_cloud->points[i]);
						aux_cloud->width++;
					}
				}
			}
			
			if(counter == 0) {
				//Start with the bag's first cloud
				final_cloud = aux_cloud;
			}
			else if(counter == std::atoi(argv[3])) break; //Limiting number of PointClouds
			else {
				//Declaring an Iterative Closest Point object
				pcl::IterativeClosestPointNonLinear<pcl::PointXYZI, pcl::PointXYZI> icp;
				icp.setTransformationEpsilon(0.001);
				icp.setInputSource(aux_cloud);
				icp.setInputTarget(final_cloud);
				//Temp cloud to receive the previous 2 clouds aligned
				pcl::PointCloud<pcl::PointXYZI> temp_cloud;
				//Making the alignment
				icp.align(temp_cloud);
				
				//Check convergence
				if(icp.hasConverged()) {
					//Join clouds
					final_cloud->width += temp_cloud.width;
					int i = 0;
					BOOST_FOREACH(pcl::PointXYZI point, temp_cloud.points) {
						final_cloud->points.push_back(temp_cloud.points[i]);
						i++;
					}
				}
				else {
					std::cerr << "Iterative Closest Point failed!" << std::endl;
					bag.close();
					return -1;
				}
			}
			counter++;
		}
		bag.close();
		
		//Save a PCD on the specified location
		pcl::io::savePCDFileASCII(argv[2], *final_cloud);
		std::cout << "Bag successfully converted to PCD. See file " << argv[2] << "." << std::endl;	
	}
	return 0;
}


